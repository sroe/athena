
/*
   Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#include "../SimHitCsvDumperAlg.h"
#include "../SpacePointCsvDumperAlg.h"
#include "../TruthSegmentCsvDumperAlg.h"
DECLARE_COMPONENT(MuonR4::SimHitCsvDumperAlg)
DECLARE_COMPONENT(MuonR4::SpacePointCsvDumperAlg)
DECLARE_COMPONENT(MuonR4::TruthSegmentCsvDumperAlg)