/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#ifndef MUONDIGITCONTAINER_MUONDIGITCONTAINERDICT_H
#define MUONDIGITCONTAINER_MUONDIGITCONTAINERDICT_H

// container
#include "MuonDigitContainer/MdtDigitContainer.h"
#include "MuonDigitContainer/RpcDigitContainer.h"
#include "MuonDigitContainer/TgcDigitContainer.h"
#include "MuonDigitContainer/CscDigitContainer.h"
#include "MuonDigitContainer/MmDigitContainer.h"
#include "MuonDigitContainer/sTgcDigitContainer.h"

// collection
#include "MuonDigitContainer/MdtDigitCollection.h"
#include "MuonDigitContainer/RpcDigitCollection.h"
#include "MuonDigitContainer/TgcDigitCollection.h"
#include "MuonDigitContainer/CscDigitCollection.h"
#include "MuonDigitContainer/MmDigitCollection.h"
#include "MuonDigitContainer/sTgcDigitCollection.h"
#endif
