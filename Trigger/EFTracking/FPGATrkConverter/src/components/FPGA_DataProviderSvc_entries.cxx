/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include "../FPGAClusterConverter.h"
#include "../FPGAActsTrkConverter.h"
#include "../FPGAConversionAlgorithm.h"

DECLARE_COMPONENT( FPGAClusterConverter)
DECLARE_COMPONENT( FPGAActsTrkConverter)
DECLARE_COMPONENT( FPGAConversionAlgorithm)

