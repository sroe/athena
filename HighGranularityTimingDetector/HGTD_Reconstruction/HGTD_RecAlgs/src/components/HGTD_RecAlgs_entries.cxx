#include "../PadClusterizationAlg.h"
#include "../TrackTimeExtensionAlg.h"
#include "../TrackTimeDefAndQualityAlg.h"
#include "../VertexTimeAlg.h"

DECLARE_COMPONENT(HGTD::PadClusterizationAlg)
DECLARE_COMPONENT(HGTD::TrackTimeExtensionAlg)
DECLARE_COMPONENT(HGTD::TrackTimeDefAndQualityAlg)
DECLARE_COMPONENT(HGTD::VertexTimeAlg)
